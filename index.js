//alert("Hello, B176!");

//Exponent Operator
/*
	An exponent operator has been added to simplify the calculation for the exponent of a given number	

*/

const firstNum = 8 ** 3
console.log(firstNum)

//Before ES6 Update
const secondNum = Math.pow(8, 3)
console.log(secondNum)

//Template Literals
/*
	Allows to write strings without using the concatenation operator (+)
*/

let name = "John";

//Pre-Template Literal String
let message = "Hello " + name + "! Welcome to programming!"
console.log(message)

//Template Literal - Uses backticks(` `)
//Mini-Activity
message = `Hello ${name}! Welcome to programming!`
console.log(message)

//Multi-Line

const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 3 with the solution of ${firstNum}. 
`
console.log(anotherMessage)

const interestRate = .1
const principal = 1000

console.log(`The interest rate on your savings account is: ${principal * interestRate}`)


/*
	ARRAY DESTRUCTURING	
	
	-Allows to unpack elements in arrays into distinct variables
	-Allows us to name array elements with variables instead of using the index numbers

	Syntax:
		let/const [variableA, variableB, VariableC] = array

*/

const fullName = ["Juan", "Dela", "Cruz"]

//Pre-Array Destructuring
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}. It's nice to see you!`)

//Array Destructuring
// const [firstName, middleName, lastName] = fullName;

// console.log(firstName)
// console.log(middleName)
// console.log(lastName)
//console.log(someName)

//console.log(`Hello ${firstName} ${middleName} ${lastName}`)

const grades = [98.5, 95, 92, 91]


//OBJECT DESTRUCTURING
/*
	-Allows us to unpack properties of objects into distinct variables
	-Shortens the syntax for accessing properties from objects

	Syntax:
		let/const {propertyName, propertyName} = object;

*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

//Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}`)

//Object Destructure

const { familyName, givenName, maidenName } = person
console.log(givenName)
console.log(maidenName)
console.log(familyName)

function getFullName({ givenName, maidenName, familyName }) {
	console.log(`${givenName} ${maidenName} ${familyName}`)
	console.log(givenName, maidenName, familyName)
}

getFullName(person)


//Arrow Functions
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code

	Syntax:
		const variableName = () => {
			//code block
		}

*/

const hello = () => console.log("Hello World")


function hello1() {
	console.log("Hello World")
}

hello()
hello1()

/*
	Pre-Arrow Function and Template Literals

	Syntax:
		function functionName (parameterA, parameterB) {
			//console.log()
		}

*/

// function printFullName(firstName, middleInitial, lastName) {
// 	console.log(firstName + " " + middleInitial + " " + lastName)
// }

// printFullName("John Michael", "V.", "Montano")

//Arrow Function
/*
	Syntax:
		let/const variableName = (parameterA, parameterB) => {
			//console.log()
		}

*/

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printFullName("Elon", "V.", "Musk")

const students = ["Joquin", "Kim", "Earl", "Noro"]
//Pre-Arrow Functions

students.forEach(function (student) {
	console.log(student + " is a student.")
})

//Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student.`)
})


//Pre-Arrow Function
// function add(x, y) {
// 	return x + y
// }

// let total = add(546, 865)
// console.log(total)

//Arrow Function
// const add  = (x, y) => {
// 	return x + y
// }

const add = (x, y) => x + y

let total = add(546, 865)
console.log(total)

//Default Function Argument Value
//Provides a default argument value if none is provided when the function is invoked

const greet = (name = 'User') => {
	return `Good morning, ${name}!`
}

console.log(greet())
console.log(greet("Batch 176"))

//Class-Based Object Blueprints
/*
	-Allows creation/instantiation of objects using classes as blueprints
	
	Syntax:
		class className {
				constructor(objectPropertyA, objectPropertyB) {
					this.objectPropertyA = objectPropertyA,
					this.objectPropertyB = objectPropertyB
				}
		}

	Instantiating an Object
	Syntax:
		let/const variableName = new ClassName()
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand,
			this.name = name,
			this.year = year
	}
}

const myCar = new Car()
console.log(myCar)

myCar.brand = "Ford"
myCar.name = "Ranger Raptor"
myCar.year = 2021

console.log(myCar)

const myNewCar = new Car("Toyota", "Vios", 2021)
console.log(myNewCar)